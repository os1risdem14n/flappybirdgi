using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite silverImage;
    public Sprite goldenImage;
    public Sprite bronzeImage;
    public int score = 25;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (score >50 && score <100)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = silverImage;
        }
        if (score > 100)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = goldenImage;
        }
        if (score < 50)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = bronzeImage;
        }
    }
}
