using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LeaderboardController : MonoBehaviour
{
    public Text first;
    public Text second;
    public Text third;
    // Start is called before the first frame update
    void Start()
    {
        first.text = PlayerPrefs.GetInt("1st") + "";
        second.text = PlayerPrefs.GetInt("2nd") + "";
        third.text = PlayerPrefs.GetInt("3rd") + "";
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ReturnMenu()
    {
        SceneManager.LoadScene(0);
    }
}
