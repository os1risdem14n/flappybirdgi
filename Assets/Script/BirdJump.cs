using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdJump : MonoBehaviour
{
    [SerializeField] float forcePerEachJump = 100f;
    public bool isDead;
    private AudioSource source;
    public AudioClip flySound;
    public AudioClip deadSound;
    public AudioClip endSound;
    //Start is called before the first frame update
    void Start()
    {
       source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        var rigidbody = GetComponent<Rigidbody2D>();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var force = new Vector2(0, forcePerEachJump);
            rigidbody.AddForce(force);
            source.PlayOneShot(flySound);
        }        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        source.PlayOneShot(deadSound);
        source.PlayOneShot(endSound);
        isDead = true;       
    }
}
