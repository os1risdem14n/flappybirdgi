using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneTupe : MonoBehaviour
{
    [SerializeField] float queue = 2.5f;
    [SerializeField] float height;
    float time;
    [SerializeField] GameObject Tupe;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time > queue)
        {
            GameObject clone = Instantiate(Tupe);
            clone.transform.position = transform.position + new Vector3(0, Random.Range(-height, height), 0);
            time = 0;
            Destroy(clone, 10);
        }
        time += Time.deltaTime;
    }
}
