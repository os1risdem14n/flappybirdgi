using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMedal : MonoBehaviour
{
    public AudioSource source;
    public AudioClip bronze;
    public AudioClip silver;
    public AudioClip golden;
    public Sprite silverImage;
    public Sprite goldenImage;
    public Sprite bronzeImage;
    public Image image;
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (MainSceneController.score > 2 && MainSceneController.score <= 5)
        {
            image.sprite = silverImage;
            source.PlayOneShot(silver);
        }
        if (MainSceneController.score > 5)
        {
            image.sprite = goldenImage;
            source.PlayOneShot(golden);
        }
        if (MainSceneController.score <= 2)
        {
            image.sprite = bronzeImage;
            source.PlayOneShot(bronze);
        }
    }
}
