using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStartController : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource source;
    public AudioClip buttonSound;
    public void Start()
    {
        source = GetComponent<AudioSource>();
    }
    public void StartGame()
    {
        source.PlayOneShot(buttonSound);
        //EditorSceneManager.LoadScene(0);
        SceneManager.LoadScene(1);
    }
    public void LeaderBoard()
    {
        source.PlayOneShot(buttonSound);
        //EditorSceneManager.LoadScene(3);
        SceneManager.LoadScene(3);
    }
}
