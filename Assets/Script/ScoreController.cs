using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController
{
    public static void ResetHighscore()
    {
        PlayerPrefs.SetInt("highScore", 0);
        PlayerPrefs.Save();
    }
    public int GetHighscore()
    {
        return PlayerPrefs.GetInt("highScore");   
    }
    public static void SetHighscore(int score)
    {
        PlayerPrefs.SetInt("highScore", score);
        PlayerPrefs.Save();
    }
}
