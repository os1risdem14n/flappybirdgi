using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainSceneController : MonoBehaviour
{
    public BirdJump bird;
    private AudioSource source;
    public AudioClip startSound;
    public Text scoreText;
    public static int score;
    public GameObject getReadyScene;

    void Start()
    {
        score = 0;
        getReadyScene.gameObject.SetActive(true);
        Time.timeScale = 0;
        source = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            getReadyScene.gameObject.SetActive(false);
            Time.timeScale = 1;
            source.PlayOneShot(startSound);
        }
        if (bird.isDead)
        {
            new WaitForSeconds(3);
            //EditorSceneManager.LoadScene(2);
            SceneManager.LoadScene(2);
        }

        scoreText.text = MainSceneController.score + "";
    }

}