using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
public class GameOverSceneController : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource source;
    public AudioClip buttonSound;
    public Text score;
    public Text highScore;
    public Image startButton;
   
    
    //public int[] leaderboard = {0, 0, 0, 0};
    //public Image medal;
    //public Sprite bronzeMedal;
    //public Sprite silverMedal;
    //public Sprite goldenMedal;
    void Start()
    {
        //if (MainSceneController.score > 2 && MainSceneController.score < 5)
        //{
        //    medal.sprite = silverMedal;
        //}
        //if (MainSceneController.score > 5)
        //{
        //    medal.sprite = goldenMedal;
        //}
        //if (MainSceneController.score < 2)
        //{
        //    medal.sprite = bronzeMedal;
        //}
        if (!PlayerPrefs.HasKey("1st"))
        {
            PlayerPrefs.SetInt("1st", 0);
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("2nd"))
        {
            PlayerPrefs.SetInt("2nd", 0);
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("3rd"))
        {
            PlayerPrefs.SetInt("3rd", 0);
            PlayerPrefs.Save();
        }
        int first = PlayerPrefs.GetInt("1st");
        int second = PlayerPrefs.GetInt("2nd");
        int third = PlayerPrefs.GetInt("3rd");
        int record = MainSceneController.score;
        source = GetComponent<AudioSource>();
        int[] temp = { 0, 0, 0 };
        //if (!PlayerPrefs.HasKey("highScore"))
        //{
        //    PlayerPrefs.SetInt("highScore", 0);
        //    PlayerPrefs.Save();
        //}
        //if (MainSceneController.score > PlayerPrefs.GetInt("highScore"))
        //{
        //    PlayerPrefs.SetInt("highScore", MainSceneController.score);
        //    PlayerPrefs.Save();
        //}
        //if (leaderboard[2] < MainSceneController.score)
        //{
        //    leaderboard[3] = MainSceneController.score;
        //    Array.Sort(leaderboard);
        //    Array.Reverse(leaderboard);
        //    PlayerPrefs.SetInt("1st", leaderboard[0]);
        //    PlayerPrefs.Save();
        //    PlayerPrefs.SetInt("2nd", leaderboard[1]);
        //    PlayerPrefs.Save();
        //    PlayerPrefs.SetInt("3rd", leaderboard[2]);
        //    PlayerPrefs.Save();
        //}
        //Debug.Log(leaderboard.Length);
        //Debug.Log(leaderboard);
        //Debug.Log(record);
        //Debug.Log(first);
        //Debug.Log(second);
        //Debug.Log(third);
        if (record > third)
        {
            third = record;
            temp[0] = first;
            temp[1] = second;
            temp[2] = third;
            Array.Sort(temp);
            Array.Reverse(temp);
            PlayerPrefs.SetInt("1st", temp[0]);
            PlayerPrefs.Save();
            PlayerPrefs.SetInt("2nd", temp[1]);
            PlayerPrefs.Save();
            PlayerPrefs.SetInt("3rd", temp[2]);
            PlayerPrefs.Save();
        }
        highScore.text = PlayerPrefs.GetInt("1st") + "";
        score.text = MainSceneController.score + "";
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(PlayerPrefs.GetInt("highScore"));
        //Debug.Log(MainSceneController.score);
       
    }
    //private void Awake()
    //{
    //    DontDestroyOnLoad(transform.gameObject);
    //}
    public void PlayAgain()
    {
        source.PlayOneShot(buttonSound);
        //EditorSceneManager.LoadScene(0);
        SceneManager.LoadScene(1);
    }

    public void LoadMenu()
    {
        source.PlayOneShot(buttonSound);
        //EditorSceneManager.LoadScene(1);
        SceneManager.LoadScene(0);
    }
}
