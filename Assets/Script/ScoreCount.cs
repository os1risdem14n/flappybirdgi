using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCount : MonoBehaviour
{
    private AudioSource scoreSound;
    private void Start()
    {
        scoreSound = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MainSceneController.score++;
        scoreSound.Play();
    }

    private string GetDebuggerDisplay()
    {
        return ToString();
    }
}
